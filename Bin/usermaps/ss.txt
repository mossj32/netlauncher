======================================================================
Title                 : Space Showdown
Filename              : SS.MAP
Author                : Alejandro Glavic
E-mail                : aglavic@uol.com.ar (feedback is always welcome)
ICQ                   : 71261284
Web Page              : http://www.serpico.com.ar/duke3d
Misc. Author Info     : I'm from Argentina, 16 years old.
                        (I know, I write the same in all my maps,
                        but who cares?)
Other Levels          : ADG Episode, YMF500G, EDF Secret Base,
                        The Lost Moonbase, HydroStation, Eye-Witness,
                        and Military Madness.

                        Some more maps for Borg Nukem and
                        Grins of Divinity.

Description           : Well I feel kinda tired of the sector limit
                        so I've made this small DM map with a lot of
                        sector work with the kind of architecture that
                        I always want to have in my maps. I've played
                        it with 3 bots (4 in total) and was cool
                        (of course I won ;D)

Additional Credits To : Maarten van Oostrum and YoshiYount for
                        betatesting this map and giving me nice
                        comments :)
                          
======================================================================

* Play Information *

Episode and Level #    : User map (E1L8)
Single Player          : No, I've already made a lot of single
                         player levels. You can play SP if you want
                         to look at it, or of course with Bots.
DukeMatch 2-8 Player   : Yes, was made for it.
Cooperative 2-8 Player : Why?
Difficulty Settings    : Depending on who are you playing with.
Plutonium Pak Required : Yes (sorry 1.3D users...)
New Art                : Duke has enough arts.
New Music              : None this time.
New Sound Effects      : No, they make a bigger (and annoying) download.
                         Also I like the original sounds.
New .CON Files         : No, let it have the original Duke feel...
Demos Replaced         : No, however if you record a demo with this map
                         feel free to mail me it and make my life a bit
                         happier :)

=====================================================================

* Construction *

Base                   : New level from scratch
Level Editor(s) Used   : Mapster (thanks to TerminX)
Art Editor(s) Used     : Doesn't have new art.
Construction Time      : Faster than you think... ;)
Known Bugs             : List bugs here, if any

* Where to get this MAP file *

File location          : The official download place is my own
                         website, http://www.serpico.com.ar/duke3d

                         Also I'm sure that this map will be at:
                         http://www.planetduke.com/msdn
                         http://www.planetduke.com/kef
                         http://duke-zone.tripod.com

                         If you put this map in your website, please
                         notice me via E-Mail.

=====================================================================

*Important Information*

Installation           : You know, I don't wanna explain it here...
                         (and I think don't need to do it)

Important Notes        : To use the ladder face it and press space.

======================================================================
