/*
	(C) 2016 Gary Sinitsin. See LICENSE file (MIT license).
*/
#pragma once

//#include "PhoneCommon.h"
#include <cstddef>
#include <stdint.h> // cstdint requires C++11 support
#include <cstring>
#include <stdexcept>
#include <string>
#include <vector>
#include <sstream>
#include "miniupnpc/miniupnpc.h"
#include "miniupnpc/upnpcommands.h"

template <typename T>
std::string toString(const T& x)
{
	std::ostringstream stream;
	stream << x;
	return stream.str();
}

// Represents a router (or other such NAT device) accessible via UPnP
// This class uses miniupnpc internally
class Router
{
public:
	Router(int discoveryTimeout);
	~Router();

	// Returns the local IP as reported by UPnP
	std::string getLocalAddress() const  {return localAddr;}

	// Returns the public IP as reported by UPnP
	std::string getWanAddress() const  {return wanAddr;}

	enum MapProto { MAP_TCP, MAP_UDP };

	// Returns TRUE on success, FALSE if port in use, throws if error
	bool setPortMapping(uint16_t localPort, uint16_t wanPort, MapProto protocol, const char* descript);

	// Clears previously set port mapping if any, throws if error
	void clearPortMapping();

protected:
	UPNPUrls upnpUrls;
	IGDdatas upnpData;
	char     localAddr[64];
	char     wanAddr[64];
	uint16_t   mappedPort;
	MapProto mappedProto;
	static const char* getProtoStr(MapProto proto)  {return (proto == MAP_TCP) ? "TCP" : "UDP";}
};
