#include "downloader.h"

void LoadBar(unsigned curr_val, unsigned max_val, unsigned bar_width)
{
	if ((curr_val != max_val) && (curr_val % (max_val / 100) != 0))
		return;

	double   ratio = curr_val / (double)max_val;
	unsigned bar_now = (unsigned)(ratio * bar_width);

	printf("\r");
	colorprintf(C_BOLD C_CYAN "%d", (unsigned)(ratio * 100.0));
	printf("%%");
	colorprintf(C_WHITE " [" C_GREEN);

	for (unsigned curr_val = 0; curr_val < bar_now; ++curr_val)
		colorprintf("=");
	for (unsigned curr_val = bar_now; curr_val < bar_width; ++curr_val)
		colorprintf(" ");

	colorprintf(C_WHITE "] " C_RESET);

	fflush(stdout);
}

bool DownloadModFile(const char* filename)
{
	const char* allowedExtensions[] = {
		"grp",
		"pk3",
		"bpk",
	};

	bool foundExt = false;
	for (int32_t i = 0; i < ARRAY_SIZE(allowedExtensions); i++)
	{
		if (stristr(filename, allowedExtensions[i]))
		{
			foundExt = true;
			break;
		}
	}

	if (!foundExt)
	{
		printf("Tried to download file of invalid extension!\n");
		return false;
	}

	char path[MAX_PATH] = "";
	char url[MAX_PATH] = "";

	sprintf(path, MODS_DIR "/%s", filename);
	sprintf(url, DOWNLOAD_URL "%s", filename);

	colorprintf(C_BOLD C_MAGENTA "-- DOWNLOAD --\n" C_RESET);
	printf(" * From: %s\n * To: %s\n", url, path);

#ifdef _WIN32
    DownloadCallback pCallback;

	HRESULT hr = URLDownloadToFile(NULL, url, path, 0, &pCallback);
	if (SUCCEEDED(hr))
	{
		colorprintf(C_BOLD C_GREEN "Download Succeeded.\n" C_RESET);
		return true;
	}
	else
	{
		colorprintf(C_BOLD C_RED "Download Failed.\n" C_RESET);
		return false;
	}
#else
	colorprintf(C_BOLD C_RED"This function is not yet supported on this platform! Sorry.\n" C_RESET);
#endif

	return false;
}