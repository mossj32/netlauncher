#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include "NetLauncher.h"

#define DOWNLOAD_URL "https://shadowmavericks.com/redirect/duke/getfile.php?download_file="

void LoadBar(unsigned curr_val, unsigned max_val, unsigned bar_width = 20);
bool DownloadModFile(const char* filename);

#ifdef _WIN32
class DownloadCallback : public IBindStatusCallback
{
private:
    double m_percentLast;
public:
    DownloadCallback() : m_percentLast(0.0) { }

    ~DownloadCallback() { }

    // This one is called by URLDownloadToFile
    STDMETHOD(OnProgress)(/* [in] */ ULONG ulProgress, /* [in] */ ULONG ulProgressMax, /* [in] */ ULONG ulStatusCode, /* [in] */ LPCWSTR wszStatusText)
    {
        switch (ulStatusCode)
        {
            case BINDSTATUS_FINDINGRESOURCE:
                printf("Finding resource...\n");
                break;
            case BINDSTATUS_CONNECTING:
                printf("Connecting...\n");
                break;
            case BINDSTATUS_SENDINGREQUEST:
                printf("Sending request...\n");
                break;
            case BINDSTATUS_MIMETYPEAVAILABLE:
                printf("Mime type available\n");
                break;
            case BINDSTATUS_CACHEFILENAMEAVAILABLE:
                printf("Cache filename available\n");
                break;
            case BINDSTATUS_BEGINDOWNLOADDATA:
                printf("Begin download\n");
                break;
            case BINDSTATUS_DOWNLOADINGDATA:
            case BINDSTATUS_ENDDOWNLOADDATA:
            {
                int percent = (int)(100.0 * static_cast<double>(ulProgress)
                    / static_cast<double>(ulProgressMax));
                if (m_percentLast < percent)
                {
                    LoadBar(percent, 100);
                    m_percentLast = percent;
                }
                if (ulStatusCode == BINDSTATUS_ENDDOWNLOADDATA)
                {
                    printf("- DONE!\n");
                }
            }
            break;

            default:
            {
                printf("Status code : %lu\n", ulStatusCode);
            }
        }
        // The download can be cancelled by returning E_ABORT here
        // of from any other of the methods.
        return S_OK;
    }

    // The rest  don't do anything...
    STDMETHOD(OnStartBinding)(/* [in] */ DWORD dwReserved, /* [in] */ IBinding __RPC_FAR* pib)
    {
        return E_NOTIMPL;
    }

    STDMETHOD(GetPriority)(/* [out] */ LONG __RPC_FAR* pnPriority)
    {
        return E_NOTIMPL;
    }

    STDMETHOD(OnLowResource)(/* [in] */ DWORD reserved)
    {
        return E_NOTIMPL;
    }

    STDMETHOD(OnStopBinding)(/* [in] */ HRESULT hresult, /* [unique][in] */ LPCWSTR szError)
    {
        return E_NOTIMPL;
    }

    STDMETHOD(GetBindInfo)(/* [out] */ DWORD __RPC_FAR* grfBINDF, /* [unique][out][in] */ BINDINFO __RPC_FAR* pbindinfo)
    {
        return E_NOTIMPL;
    }

    STDMETHOD(OnDataAvailable)(/* [in] */ DWORD grfBSCF, /* [in] */ DWORD dwSize, /* [in] */ FORMATETC __RPC_FAR* pformatetc, /* [in] */ STGMEDIUM __RPC_FAR* pstgmed)
    {
        return E_NOTIMPL;
    }

    STDMETHOD(OnObjectAvailable)(/* [in] */ REFIID riid, /* [iid_is][in] */ IUnknown __RPC_FAR* punk)
    {
        return E_NOTIMPL;
    }

    // IUnknown stuff
    STDMETHOD_(ULONG, AddRef)()
    {
        return 0;
    }

    STDMETHOD_(ULONG, Release)()
    {
        return 0;
    }

    STDMETHOD(QueryInterface)(/* [in] */ REFIID riid, /* [iid_is][out] */ void __RPC_FAR* __RPC_FAR* ppvObject)
    {
        return E_NOTIMPL;
    }
};
#endif

#endif // DOWNLOADER_H
