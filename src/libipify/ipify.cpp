#ifdef _WIN32
#ifndef _WINSOCK_DEPRECATED_NO_WARNINGS
	#define _WINSOCK_DEPRECATED_NO_WARNINGS
#endif

	#include <WinSock2.h>
	#include <ws2tcpip.h>
#else
	#include <arpa/inet.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <netdb.h>
	#include <unistd.h>
#endif
#include <string.h>
#include <stdio.h>
#include <iostream>
#include "ipify.h"

using std::cout;
using std::string;

#define IPIFY_HOST    "api.ipify.org"

char seps[] = " ,\t\n";

void get_ip(char *ip)
{
	string website_HTML;
	char buffer[10000];
	char lineBuffer[200][80] = { ' ' };
	int lineIndex = 0;

	SOCKET Socket;
	SOCKADDR_IN SockAddr;
	int lineCount = 0;
	int rowCount = 0;
	struct hostent *host;
	char *get_http = new char[256];

	memset(buffer, '\0', sizeof(buffer));

	memset(get_http, ' ', sizeof(get_http));
	strcpy(get_http, "GET / HTTP/1.1\r\nHost: ");
	strcat(get_http, IPIFY_HOST);
	strcat(get_http, "\r\nConnection: close\r\n\r\n");

#ifdef _WIN32
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
		cout << "WSAStartup failed.\n";
		system("pause");
		//return 1;
	}
#endif

	Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	host = gethostbyname(IPIFY_HOST);

	SockAddr.sin_port = htons(80);
	SockAddr.sin_family = AF_INET;
	SockAddr.sin_addr.s_addr = *((unsigned long*)host->h_addr);

	if (connect(Socket, (SOCKADDR*)(&SockAddr), sizeof(SockAddr)) != 0) {
		cout << "Could not connect";
		system("pause");
		//return 1;
	}
	send(Socket, get_http, strlen(get_http), 0);

	int nDataLength;
	while ((nDataLength = recv(Socket, buffer, 10000, 0)) > 0) {
		int i = 0;
		while (buffer[i] >= 32 || buffer[i] == '\n' || buffer[i] == '\r') {

			website_HTML += buffer[i];
			i += 1;
		}
	}

	closesocket(Socket);

#ifdef _WIN32
	WSACleanup();
#endif

	delete[] get_http;

	for (size_t i = 0; i < website_HTML.length(); ++i)
	{
		website_HTML[i] = tolower(website_HTML[i]);
	}

	char *token = strtok(buffer, seps);
	while (token != NULL) {

		strcpy(lineBuffer[lineIndex], token);
		int dot = 0;
		for (size_t ii = 0; ii < strlen(lineBuffer[lineIndex]); ii++) {

			if (lineBuffer[lineIndex][ii] == '.') dot++;
			if (dot >= 3) {
				dot = 0;
				strcpy(ip, lineBuffer[lineIndex]);
			}
		}

		token = strtok(NULL, seps);
		lineIndex++;
	}
}
